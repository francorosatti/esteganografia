/***************************************************************************
* Title:  Esteganografia - Procesamiento Digital de Imagenes
* Author: Bonillo - Longo - Rosatti
* Verion: 4.0
* Date:   May 2016
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <windows.h>
#include <math.h>
#include <opencv2\core\core_c.h>
#include <opencv2\highgui\highgui_c.h>
#include <opencv2\imgproc\imgproc_c.h>

//Defines
#define MAX_FILE_NAME 1000

//Procesamiento de Imagenes
void EmbedImage(IplImage* Dst, IplImage* P, IplImage* I);
void EmbedImageMultiBit(IplImage* Dst, IplImage* P, IplImage* I);
void EmbedTextMultiBit(IplImage* Dst, IplImage* P, FILE* I);
void EmbedByte(unsigned long long* Dst, unsigned long long* Prt, unsigned char* Inf);
void EmbedByteMultiBit(unsigned char* Dst, unsigned char* Prt, unsigned char* Inf, unsigned int* offset, unsigned int cbits);
void DeEmbedImage(IplImage* O, IplImage* Src);
void DeEmbedImageMultiBit(IplImage* O, IplImage* Src);
void DeEmbedTextMultiBit(FILE* Dst, IplImage* Src);
void DeEmbedByte(unsigned char* Dst, unsigned long long* Src);
void DeEmbedByteMultiBit(unsigned char* Dst, unsigned char* Src, unsigned int* offset, unsigned int cbits);
CvSize GetImageSize(IplImage* Img);
unsigned int GetTextSize(IplImage* Img);

//Interfaz de Usuario
void Header(void);
void Menu1(void);
void Menu2(void);
void Menu3(void);
void Resultados(IplImage* portadora, IplImage* informacion, IplImage* salida, IplImage* recuperada);
void Resultados(IplImage* portadora, FILE* informacion, IplImage* salida, FILE* recuperada);

//Utilidades
int OpenFileDialog(int textFileAllowed);
void PathToWindows(TCHAR* text);
int GetHistograma(IplImage *img, unsigned int *Histovector);
int GetMaximo(unsigned int a[], unsigned int cant);
void Vaciar(unsigned int a[], unsigned int cant);
void Vaciar(char* ptr, unsigned int cant, unsigned int tamanio);
int GetFileSize(char * filename);

//Tipos de Datos
union Chunk8
{
	unsigned long long l;
	unsigned char c[8];
};
union Tamanio
{
	unsigned int i[2];
	unsigned char c[8];
};
