/***************************************************************************
* Title : Esteganografia - Procesamiento Digital de Imagenes
* Author: Bonillo - Longo - Rosatti
* Verion: 4.1
* Date  : Jun 2016
****************************************************************************/

#include "main.h"

/***************************************************************************
*								INICIO MAIN
****************************************************************************/

//Variables
char NombreArchivo[MAX_FILE_NAME];
int flag = 0;

//Punteros
IplImage* Portadora;
IplImage* I_Informacion;
FILE* T_Informacion;
IplImage* Salida;
IplImage* Entrada;
IplImage* I_Recuperada;
FILE* T_Recuperada;
unsigned char cbits = 1;

int main(void)
{
	IplImage *imgbyn = NULL, *imghistogram1 = NULL, *imghistogram2 = NULL;
	FILE* fp = NULL;
	unsigned int histovector1[256] = { 0 }, histovector2[256] = { 0 };
	int err = 0, option = 0, aux = 0, max1 = 0, max2 = 0, PortadoraSeleccionada = 0, InformacionSeleccionada = 0, InformacionIsTexto = 0;
	char *ext = NULL, *str = NULL;
	float val;

	//Inicio
	Header();
	Menu1();
	while (!flag) {
		printf("\n\tIngrese la opcion deseada: ");
		fflush(stdin);
		option = 0;
		scanf_s("%d", &option);
		switch (option) {
		case 1:
			while(OpenFileDialog(0) == 0){}

			//Abrimos la imagen portadora a procesar
			Portadora = cvLoadImage(NombreArchivo, CV_LOAD_IMAGE_GRAYSCALE);

			//Chequeamos que se hayan abierto correctamente
			if(Portadora == NULL)
			{
				printf("\tNo fue posible abrir la imagen Portadora\n");
				system("PAUSE");
				return EXIT_FAILURE;
			}
			
			cvSaveImage("Portadora.png", Portadora);
			cvReleaseImage(&Portadora);
			Portadora = cvLoadImage("Portadora.png", CV_LOAD_IMAGE_GRAYSCALE);
			str = strrchr(NombreArchivo, '/');
			printf("\tLa imagen Portadora '%s' se cargo exitosamente.\n",str + 1);
			PortadoraSeleccionada = 1;
			break;

		case 2:
			while(OpenFileDialog(1) == 0){}

			ext = strrchr(NombreArchivo, '.');
			if(strcmp(ext , ".txt") == 0) InformacionIsTexto = 1; else InformacionIsTexto = 0;

			if(InformacionIsTexto == 1)
			{
				err = fopen_s(&T_Informacion, NombreArchivo, "rb");
				if(T_Informacion == NULL || err != 0)
				{
					printf("\tNo fue posible abrir el archivo Informacion\n");
					system("PAUSE");
					return EXIT_FAILURE;
				}

				err = fopen_s(&fp, "Informacion.txt", "wb+");
				if (fp == NULL || err != 0) {
					fclose(fp);
					printf("\tNo fue posible crear el archivo Informacion\n");
					system("PAUSE");
					return EXIT_FAILURE;
				}
				char chr;
				do {
					chr = fgetc(T_Informacion);
					if(chr != EOF)fputc(chr, fp);
				} while (chr != EOF);
				fclose(fp);

				str = strrchr(NombreArchivo, '/');
				printf("\tEl archivo Informacion '%s' se cargo exitosamente.\n", str + 1);
			}
			else
			{
				//Abrimos la imagen informacion a procesar
				I_Informacion = cvLoadImage(NombreArchivo, CV_LOAD_IMAGE_GRAYSCALE);
				if(I_Informacion == NULL)
				{
					printf("\tNo fue posible abrir la imagen Informacion\n");
					system("PAUSE");
					return EXIT_FAILURE;
				}

				cvSaveImage("Informacion.png", I_Informacion);
				str = strrchr(NombreArchivo, '/');
				printf("\tLa imagen Informacion '%s' se cargo exitosamente.\n", str + 1);
			}

			InformacionSeleccionada = 1;
			break;

		case 3:
			//Chequeamos que ya se hayan seleccionado las dos imagenes necesarias para el proceso
			if(PortadoraSeleccionada == 0 || InformacionSeleccionada == 0)
			{
				printf("\tSeleccione una imagen Portadora y una imagen o archivo Informacion para poder realizar el procesamiento\n");
				break;
			}

			if(InformacionIsTexto)
			{
				fseek(T_Informacion, 0, SEEK_SET);
				fseek(T_Informacion, 0, SEEK_END);
				unsigned int size = ftell(T_Informacion);
				fseek(T_Informacion, 0, SEEK_SET);
				//Chequeamos que los tama�os sean correctos para poder aplicar el proceso
				if((unsigned int)Portadora->imageSize < (size + 10))
				{
					printf("\tLa imagen Portadora no tiene el tama�o suficiente para embeber la imagen Infomacion\n");
					break;
				}

				//Creamos la imagen de salida a partir de la 'Portadora'
				Salida = cvCloneImage(Portadora);

				//Chequeamos que la imagen de salida se haya creado bien
				if(Salida == NULL)
				{
					printf("\tNo fue posible crear la imagen Salida\n");
					system("PAUSE");
					return EXIT_FAILURE;
				}

				//Procesamos las imagenes
				EmbedTextMultiBit(Salida, Portadora, T_Informacion);
				cvSaveImage("Salida.png", Salida);

				//Levantamos la imagen de salida desde el archivo
				Entrada = cvLoadImage("Salida.png", 0);

				//Chequeamos que la imagen de entrada se haya abierto bien
				if(Entrada == NULL)
				{
					printf("\tNo fue posible abrir la imagen Entrada\n");
					system("PAUSE");
					return EXIT_FAILURE;
				}

				err = fopen_s(&T_Recuperada, "Recuperada.txt", "wb");
				if (fp == NULL || err != 0) {
					fclose(fp);
					printf("\tNo fue posible crear el archivo Recuperada\n");
					system("PAUSE");
					return EXIT_FAILURE;
				}

				//Recuperamos el archivo informacion
				DeEmbedTextMultiBit(T_Recuperada, Entrada);
				fclose(T_Informacion);
				fclose(T_Recuperada);
			}
			else
			{
				//Chequeamos que los tama�os de ambas imagenes sean correctos para poder aplicar el proceso
				if(Portadora->imageSize < (I_Informacion->imageSize + 10))
				{
					printf("\tLa imagen Portadora no tiene el tama�o suficiente para embeber la imagen Infomacion\n");
					break;
				}

				//Creamos la imagen de salida a partir de la 'Portadora'
				Salida = cvCloneImage(Portadora);

				//Chequeamos que la imagen de salida se haya creado bien
				if(Salida == NULL)
				{
					printf("\tNo fue posible crear la imagen Salida\n");
					system("PAUSE");
					return EXIT_FAILURE;
				}

				//Procesamos las imagenes
				EmbedImageMultiBit(Salida, Portadora, I_Informacion);
				cvSaveImage("Salida.png", Salida);

				//Levantamos la imagen de salida desde el archivo
				Entrada = cvLoadImage("Salida.png", 0);

				//Chequeamos que la imagen de entrada se haya abierto bien
				if(Entrada == NULL)
				{
					printf("\tNo fue posible abrir la imagen Entrada\n");
					system("PAUSE");
					return EXIT_FAILURE;
				}

				//Creamos una imagen del tama�o de la imagen embebida
				//El tama�o lo recuperamos con la funcion GetSize
				I_Recuperada = cvCreateImage(GetImageSize(Entrada), IPL_DEPTH_8U, 1);

				//Chequeamos que la imagen recuperada se haya creado bien
				if(I_Recuperada == NULL)
				{
					printf("\tNo fue posible crear la imagen Recuperada\n");
					system("PAUSE");
					return EXIT_FAILURE;
				}

				//Recuperamos la imagen informacion
				DeEmbedImageMultiBit(I_Recuperada, Entrada);
				//Guardamos la imagen recuperada para luego poder comparar si fue recuperada correctamente
				cvSaveImage("Recuperada.png", I_Recuperada);
			}
			flag = 1;
			break;

		case 4:
			return EXIT_SUCCESS;
			break;

		default:
			printf("\tOpcion inexistente, ingrese una opcion valida.\n");
			break;
		}
	}
	
	flag = 0;
	
	if(!InformacionIsTexto)
	{
		Menu2();

		while(!flag){

			printf("\n\tIngrese la opcion deseada: ");
			fflush(stdin);
			option = 0;
			scanf_s("%d", &option);
			switch (option) {
			case 1:
				Portadora = cvLoadImage("Portadora.png", CV_LOAD_IMAGE_GRAYSCALE);
				Salida = cvLoadImage("Salida.png", CV_LOAD_IMAGE_GRAYSCALE);
				cvNamedWindow("Portadora", CV_WINDOW_NORMAL);
				cvNamedWindow("Salida", CV_WINDOW_NORMAL);
				cvShowImage("Portadora", Portadora);
				cvShowImage("Salida", Salida);
				cvWaitKey(0);
				cvDestroyWindow("Portadora");
				cvDestroyWindow("Salida");
				break;
			case 2:
				I_Informacion = cvLoadImage("Informacion.png", CV_LOAD_IMAGE_GRAYSCALE);
				I_Recuperada = cvLoadImage("Recuperada.png", CV_LOAD_IMAGE_GRAYSCALE);
				cvNamedWindow("Informacion", CV_WINDOW_AUTOSIZE);
				cvNamedWindow("Recuperada", CV_WINDOW_AUTOSIZE);
				cvShowImage("Informacion", I_Informacion);
				cvShowImage("Recuperada", I_Recuperada);
				cvWaitKey(0);		
				cvDestroyWindow("Informacion");
				cvDestroyWindow("Recuperada");
				break;
			case 3:
				Vaciar(histovector1, 256);
				Vaciar(histovector2, 256);
				max1 = GetHistograma(Portadora, histovector1);
				max2 = GetHistograma(Salida, histovector2);
				imghistogram1 = cvCreateImage(cvSize(512, 512), IPL_DEPTH_8U, 1);
				imghistogram2 = cvCreateImage(cvSize(512, 512), IPL_DEPTH_8U, 1);
				cvNamedWindow("Histograma Portadora", CV_WINDOW_AUTOSIZE);
				cvNamedWindow("Histograma Salida", CV_WINDOW_AUTOSIZE);
				for (aux = 0; aux < 512; aux += 2) {			/* Dibujo imagen histograma */
					val = ((float)(histovector1[aux / 2] * 510))/((float)max1);
					cvLine(imghistogram1, cvPoint(aux, 511), cvPoint(aux, 511 - unsigned int(val)), cvScalar(255, 255, 255, 1), 1, 8, 0);
					val = ((float)(histovector2[aux / 2] * 510))/((float)max2);
					cvLine(imghistogram2, cvPoint(aux, 511), cvPoint(aux, 511 - unsigned int(val)), cvScalar(255, 255, 255, 1), 1, 8, 0);
				}
				cvShowImage("Histograma Portadora", imghistogram1);
				cvShowImage("Histograma Salida", imghistogram2);
				cvWaitKey(0);
				cvDestroyWindow("Histograma Portadora");
				cvDestroyWindow("Histograma Salida");
				break;
			case 4:
				Vaciar(histovector1, 256);
				Vaciar(histovector2, 256);
				max1 = GetHistograma(I_Informacion, histovector1);
				max2 = GetHistograma(I_Recuperada, histovector2);
				imghistogram1 = cvCreateImage(cvSize(512, 512), IPL_DEPTH_8U, 1);
				imghistogram2 = cvCreateImage(cvSize(512, 512), IPL_DEPTH_8U, 1);
				cvNamedWindow("Histograma Informacion", CV_WINDOW_NORMAL);
				cvNamedWindow("Histograma Recuperada", CV_WINDOW_NORMAL);
				for (aux = 0; aux < 512; aux += 2) {			/* Dibujo imagen histograma */
					val = ((float)(histovector1[aux / 2] * 510))/((float)max1);
					cvLine(imghistogram1, cvPoint(aux, 511), cvPoint(aux, 511 - unsigned int(val)), cvScalar(255, 255, 255, 1), 1, 8, 0);
					val = ((float)(histovector2[aux / 2] * 510))/((float)max2);
					cvLine(imghistogram2, cvPoint(aux, 511), cvPoint(aux, 511 - unsigned int(val)), cvScalar(255, 255, 255, 1), 1, 8, 0);
				}
				cvShowImage("Histograma Informacion", imghistogram1);
				cvShowImage("Histograma Recuperada", imghistogram2);
				cvWaitKey(0);
				cvDestroyWindow("Histograma Informacion");
				cvDestroyWindow("Histograma Recuperada");
				break;
			case 5:
				Resultados(Portadora, I_Informacion, Salida, I_Recuperada);
				system("PAUSE");
				Menu2();
				break;
			case 6:
				flag = 1;
				break;
			default:
				printf("\n\tOpcion inexistente, ingrese una opcion valida.\n");
				break;
			}
		}
	}
	else
	{
		Menu3();

		while(!flag){

			printf("\n\tIngrese la opcion deseada: ");
			fflush(stdin);
			option = 0;
			int c = 0, errcnt = 0;
			scanf_s("%d", &option);
			switch (option) {
			case 1:
				Portadora = cvLoadImage("Portadora.png", CV_LOAD_IMAGE_GRAYSCALE);
				Salida = cvLoadImage("Salida.png", CV_LOAD_IMAGE_GRAYSCALE);
				cvNamedWindow("Portadora", CV_WINDOW_NORMAL);
				cvNamedWindow("Salida", CV_WINDOW_NORMAL);
				cvShowImage("Portadora", Portadora);
				cvShowImage("Salida", Salida);
				cvWaitKey(0);
				cvDestroyWindow("Portadora");
				cvDestroyWindow("Salida");
				break;
			case 2:
				system("cls");		// Windows
				//system("clear");	// Linux
				printf("\n\n\t\t\tComparacion de Texto Informacion y Texto Recuperado\n");
				printf("\n\tInformacion: ");
				err = fopen_s(&T_Informacion, "Informacion.txt", "r");
				if(err != 0)
				{
					printf("No fue posible abrir el archivo de texto de informacion");
					system("PAUSE");
					Menu3();
					break;
				}
				err = fopen_s(&T_Recuperada, "Recuperada.txt", "r");
				if(err != 0)
				{
					printf("No fue posible abrir el archivo de texto recuperado");
					system("PAUSE");
					Menu3();
					break;
				}
				while((c = fgetc(T_Informacion)) != EOF) {
					if(c != fgetc(T_Recuperada))errcnt++;
					printf("%c",(char)c);
				}
				fclose(T_Informacion);
				c = 0;
				err = fopen_s(&T_Recuperada, "Recuperada.txt", "r");
				printf("\n\n\tRecuperado : ");
				while((c = fgetc(T_Recuperada)) != EOF) {
					printf("%c",(char)c);
				}
				printf("\n\nCantidad de caracteres erroneos: %d\n\n", errcnt);
				fclose(T_Recuperada);
				system("PAUSE");
				Menu3();
				break;
			case 3:
				Vaciar(histovector1, 256);
				Vaciar(histovector2, 256);
				max1 = GetHistograma(Portadora, histovector1);
				max2 = GetHistograma(Salida, histovector2);
				imghistogram1 = cvCreateImage(cvSize(512, 512), IPL_DEPTH_8U, 1);
				imghistogram2 = cvCreateImage(cvSize(512, 512), IPL_DEPTH_8U, 1);
				cvNamedWindow("Histograma Portadora", CV_WINDOW_AUTOSIZE);
				cvNamedWindow("Histograma Salida", CV_WINDOW_AUTOSIZE);
				for (aux = 0; aux < 512; aux += 2) {			/* Dibujo imagen histograma */
					val = ((float)(histovector1[aux / 2] * 510))/((float)max1);
					cvLine(imghistogram1, cvPoint(aux, 511), cvPoint(aux, 511 - unsigned int(val)), cvScalar(255, 255, 255, 1), 1, 8, 0);
					val = ((float)(histovector2[aux / 2] * 510))/((float)max2);
					cvLine(imghistogram2, cvPoint(aux, 511), cvPoint(aux, 511 - unsigned int(val)), cvScalar(255, 255, 255, 1), 1, 8, 0);
				}
				cvShowImage("Histograma Portadora", imghistogram1);
				cvShowImage("Histograma Salida", imghistogram2);
				cvWaitKey(0);
				cvDestroyWindow("Histograma Portadora");
				cvDestroyWindow("Histograma Salida");
				break;
			case 4:
				printf("\n\tOpcion disponible unicamente para archivos de Informacion del tipo imagen.\n");
				break;
			case 5:
				Resultados(Portadora, T_Informacion, Salida, T_Recuperada);
				system("PAUSE");
				Menu3();
				break;
			case 6:
				flag = 1;
				break;
			default:
				printf("\n\tOpcion inexistente, ingrese una opcion valida.\n");
				break;
			}
		}
	}
	
	//Cerramos todas las ventamos y liberamos memoria
	cvDestroyAllWindows();
	cvReleaseImage(&imghistogram1);
	cvReleaseImage(&imghistogram2);
	cvReleaseImage(&Portadora);
	cvReleaseImage(&I_Informacion);
	cvReleaseImage(&Salida);
	cvReleaseImage(&Entrada);
	cvReleaseImage(&I_Recuperada);
	T_Informacion = NULL;
	T_Recuperada = NULL;

	return EXIT_SUCCESS;
}

/***************************************************************************
*								FIN MAIN
****************************************************************************/

/***************************************************************************
*						INICIO PROCESAMIENTO DE IMAGENES
****************************************************************************/

/***************************************************************************
Header de embebido:
	8 bytes para la cantidad de bits menos significativos utilizados para embeber
	8 bytes para el tipo: 'I' para imagenes, 'T' para archivos de texto
	32 bytes para el ancho de la imagen
	32 bytes para el alto de la imagen
****************************************************************************/

/*****************************************************
* Descripcion: Embebe la imagen informacion dentro de
*              la imagen portadora.
* Dst: Puntero a la imagen de salida
* P:   Puntero a la imagen portadora
* I:   Puntero a la imagen que se desea embeber
******************************************************/
void EmbedImage(IplImage* Dst, IplImage* P, IplImage* I)
{
	//primero escribimos el tama�o
	union Tamanio tam;
	tam.i[0] = I->width;
	tam.i[1] = I->height;
	for (int j = 0; j <= 7; j++)
	{
		EmbedByte((unsigned long long*)(Dst->imageData + j * 8), (unsigned long long*)(P->imageData + j * 8), (unsigned char*)(tam.c + j));
	}

	//luego escribimos la imagen informacion
	for (int i = 0; i < I->imageSize - 3; i++)
	{
		EmbedByte((unsigned long long*)(Dst->imageData + 64 + i * 8), (unsigned long long*)(P->imageData + 64 + i * 8), (unsigned char*)(I->imageData + i));
	}
}

void EmbedImageMultiBit(IplImage* Dst, IplImage* P, IplImage* I)
{
	cbits = (int)(8.0/((float)P->imageSize/(float)(I->imageSize + 10))) + 1;
	union Tamanio tam;
	tam.i[0] = I->width;
	tam.i[1] = I->height;
	unsigned int offset = 0;
	unsigned char type = 'I';

	EmbedByte((unsigned long long*)(Dst->imageData), (unsigned long long*)(P->imageData), &cbits);
	EmbedByte((unsigned long long*)(Dst->imageData + 8), (unsigned long long*)(P->imageData + 8), &type);

	for (int j = 0; j <= 7; j++)
	{
		EmbedByte((unsigned long long*)(Dst->imageData + j * 8 + 16), (unsigned long long*)(P->imageData + j * 8 + 16), (unsigned char*)(tam.c + j));
	}

	//luego escribimos la imagen informacion
	for (int i = 0; i < I->imageSize ; i++)
	{
		EmbedByteMultiBit((unsigned char*)(Dst->imageData + (i*8)/cbits + 80), (unsigned char*)(P->imageData + (i*8)/cbits + 80), (unsigned char*)(I->imageData + i), &offset, cbits);
	}
}

void EmbedTextMultiBit(IplImage* Dst, IplImage* P, FILE* I)
{
	fseek(I, 0, SEEK_SET);
	fseek(I, 0, SEEK_END);
	union Tamanio tam;
	tam.i[0] = ftell(I);
	tam.i[1] = 0;
	fseek(I, 0, SEEK_SET);
	cbits = (int)((float)(8*tam.i[0])/(float)(P->imageSize - 80)) + 1;
	unsigned int offset = 0;
	unsigned char type = 'T';

	EmbedByte((unsigned long long*)(Dst->imageData), (unsigned long long*)(P->imageData), &cbits);
	EmbedByte((unsigned long long*)(Dst->imageData + 8), (unsigned long long*)(P->imageData + 8), &type);

	//Embebemos el tama�o del archivo de texto
	for (int j = 0; j <= 7; j++)
	{
		EmbedByte((unsigned long long*)(Dst->imageData + j * 8 + 16), (unsigned long long*)(P->imageData + j * 8 + 16), (unsigned char*)(tam.c + j));
	}

	//Embebemos el archivo de texto
	unsigned int i = 0;
	char chr;
	do {
		chr = fgetc(I);
		if(chr != EOF)
		{
			EmbedByteMultiBit((unsigned char*)(Dst->imageData + (i*8)/cbits + 80), (unsigned char*)(P->imageData + (i*8)/cbits + 80), (unsigned char*)&chr, &offset, cbits);
			i++;
		}
	} while (chr != EOF);
}

/***************************************************************************
* Descripcion: Embebe un byte de informacion en 8 bytes de portadora.
* Dst: puntero a los 8 bytes de salida con el byte de informacion embebido
* Prt: Puntero a los 8 bytes donde se embeber� el byte Inf
* Inf: Punter al byte que se desea embeber
****************************************************************************/
void EmbedByte(unsigned long long* Dst, unsigned long long* Prt, unsigned char* Inf)
{
	int i = 0;
	union Chunk8 chk;
	chk.l = *Prt;
	for (i = 0; i < 8; i++)
	{
		if ((((0x01 << i)&(*Inf)) >> i) == 0)
		{
			chk.c[i] &= 0XFE;
		}
		else
		{
			chk.c[i] |= 0X01;
		}
	}
	*Dst = chk.l;
}

void EmbedByteMultiBit(unsigned char* Dst, unsigned char* Prt, unsigned char* Inf, unsigned int* offset, unsigned int cantbits)
{
	int i = 0;
	int j = 0;
	unsigned char chk;
	if((*offset) == 0)
	{
		chk = *Prt;
	}
	else
	{
		chk = *Dst;
	}
	for (i = 0; i < 8; i++)
	{
		if ((((0x01 << i)&(*Inf)) >> i) == 0)
		{
			chk &= (~(0X01 << (*offset)));
		}
		else
		{
			chk |= (0X01 << (*offset));
		}
		(*offset)++;
		if((*offset) >= cantbits)
		{
			(*offset) = 0;
			*(Dst + j) = chk;
			j++;
			chk = *(Prt + j);
		}
	}
	*(Dst + j) = chk;
}

/***********************************************************
* Descripcion: Desembebe la imagen informacion a partir de
*              la imagen de entrada.
* Dst: Puntero a la imagen de salida
* Src: Puntero a la imagen que contiene la imagen embebida
************************************************************/
void DeEmbedImage(IplImage* Dst, IplImage* Src)
{
	for (int i = 0; i < Dst->imageSize - 3; i++)
	{
		DeEmbedByte((unsigned char*)(Dst->imageData + i), (unsigned long long*)(Src->imageData + 64 + i * 8));
	}
}

void DeEmbedImageMultiBit(IplImage* Dst, IplImage* Src)
{
	unsigned int offset = 0;
	cbits = 1;
	DeEmbedByte(&cbits, (unsigned long long*)(Src->imageData));

	for (int i = 0; i < Dst->imageSize ; i++)
	{
		DeEmbedByteMultiBit((unsigned char*)(Dst->imageData + i), (unsigned char*)(Src->imageData + (i*8)/cbits + 80), &offset, cbits);
	}
}

void DeEmbedTextMultiBit(FILE* Dst, IplImage* Src)
{
	unsigned int offset = 0;
	cbits = 1;
	DeEmbedByte(&cbits, (unsigned long long*)(Src->imageData));
	unsigned int tam = GetTextSize(Src);
	unsigned char chr = 0;
	for (unsigned int i = 0; i < tam ; i++)
	{
		DeEmbedByteMultiBit(&chr, (unsigned char*)(Src->imageData + (i*8)/cbits + 80), &offset, cbits);
		fputc(chr,Dst);
	}
}

/***************************************************************************
* Descripcion: Desembebe un byte de informacion desde 8 bytes de la entrada.
* Dst: puntero al byte de salida
* Src: Puntero a los 8 bytes que contienen un byte embebido
****************************************************************************/
void DeEmbedByte(unsigned char* Dst, unsigned long long* Src)
{
	int i = 0;
	union Chunk8 chk;
	chk.l = *Src;
	*Dst = 0;
	for (i = 0; i < 8; i++)
	{
		*Dst |= ((0X01 & (chk.c[i])) << i);
	}
}

void DeEmbedByteMultiBit(unsigned char* Dst, unsigned char* Src, unsigned int* offset, unsigned int cantbits)
{
	int i = 0;
	int j = 0;
	unsigned char data = *Src;
	*Dst = 0;
	for (i = 0; i < 8; i++)
	{
		*Dst |= ((((0X01 << (*offset)) & data)>>(*offset)) << i);
		(*offset)++;
		if((*offset) >= cantbits)
		{
			(*offset) = 0;
			j++;
			data = *(Src + j);
		}
	}
}

/***************************************************************************
* Descripcion: retorna el tama�o de la imagen en fomato CvSize.
* Img: Puntero a la imagen que tiene embebida tanto la imagen informacion
*      como su tama�o
****************************************************************************/
CvSize GetImageSize(IplImage* Img)
{
	CvSize tam = cvSize(Img->width, Img->height);
	union Tamanio aux;
	for (unsigned int j = 0; j <= 7; j++)
	{
		DeEmbedByte(aux.c + j, (unsigned long long*)(Img->imageData + j * 8 + 16));
	}
	tam.width = aux.i[0];
	tam.height = aux.i[1];
	return tam;
}

/***************************************************************************
* Descripcion: retorna el tama�o del archivo de texto.
* Img: Puntero a la imagen que tiene embebida tanto la imagen informacion
*      como su tama�o
****************************************************************************/
unsigned int GetTextSize(IplImage* Img)
{
	unsigned int tam = 0;
	union Tamanio aux;
	for (unsigned int j = 0; j <= 7; j++)
	{
		DeEmbedByte(aux.c + j, (unsigned long long*)(Img->imageData + j * 8 + 16));
	}
	tam = aux.i[0];
	return tam;
}

/***************************************************************************
*						FIN PROCESAMIENTO DE IMAGENES
****************************************************************************/

/***************************************************************************
*						INICIO INTERFAZ DE USUARIO
****************************************************************************/

/***************************************************************************
* Descripcion: Imprime en pantalla la presentacion de la aplicacion.
****************************************************************************/
void Header()
{
	printf("********************************************************************");
	printf("\n\t\tProcesamiento Digital de Imagenes\n\n");
	printf("Tema:\t\tEsteganografia por Least Significant Bits\n\n");
	printf("Profesor:\tIng. Fernando Lage\n");
	printf("ATP:\t\tDiego Durante\n");
	printf("\nAlumnos:");
	printf("\t* Bonillo, Nicolas\n");
	printf("\t\t* Longo, Celeste\n");
	printf("\t\t* Rosatti, Franco\n\n");
	printf("********************************************************************\n\n");
	printf("\nPresione cualquier tecla para comenzar...");
	_getch();
}

/***************************************************************************
* Descripcion: Imprime en pantalla el men� de Pre-Procesamiento
****************************************************************************/
void Menu1()
{
	system("cls");		// Windows
	//system("clear");	// Linux
	printf("\n\t\t\tMenu: Pre-Procesamiento\n");
	printf("\n\t1) Seleccionar Imagen Portadora.");
	printf("\n\t2) Seleccionar Archivo Informacion.");
	printf("\n\t3) Procesar Imagenes.");
	printf("\n\t4) Salir.\n");
}

/***************************************************************************
* Descripcion: Imprime en pantalla el men� de Post-Procesamiento
* Informacion: Archivo de Imagen
****************************************************************************/
void Menu2()
{
	system("cls");		// Windows
	//system("clear");	// Linux
	printf("\n\n\t\t\tMenu: Post-Procesamiento\n");
	printf("\n\t1) Comparar Imagenes:\t Portadora y Salida.");
	printf("\n\t2) Comparar Imagenes:\t Informacion y Recuperada.");
	printf("\n\t3) Comparar Histogramas: Portadora y Salida.");
	printf("\n\t4) Comparar Histogramas: Informacion y Recuperada.");
	printf("\n\n\t5) Resumen del proceso.");
	printf("\n\t6) Salir.\n");
}

/***************************************************************************
* Descripcion: Imprime en pantalla el men� de Post-Procesamiento
* Informacion: Archivo de Texto
****************************************************************************/
void Menu3()
{
	system("cls");		// Windows
	//system("clear");	// Linux
	printf("\n\n\t\t\tMenu: Post-Procesamiento\n");
	printf("\n\t1) Comparar Imagenes:\t Portadora y Salida.");
	printf("\n\t2) Comparar Textos:\t Informacion y Recuperado.");
	printf("\n\t3) Comparar Histogramas: Portadora y Salida.");
	printf("\n\n\t5) Resumen del proceso.");
	printf("\n\t6) Salir.\n");
}

/***************************************************************************
* Descripcion: Imprime en pantalla la informaci�n resultado del procesamiento
****************************************************************************/
void Resultados(IplImage* portadora, IplImage* informacion, IplImage* salida, IplImage* recuperada)
{
	int p_size, i_size, s_size, r_size;
	float aux;

	p_size = GetFileSize("Portadora.png");
	i_size = GetFileSize("Informacion.png");
	s_size = GetFileSize("Salida.png");
	r_size = GetFileSize("Recuperada.png");

	system("cls");		// Windows
	//system("clear");	// Linux
	printf("\n\n\t\t\tResumen del Proceso\n");

	printf("\n\tPortadora:\tNombre de Archivo: Portadora.png");
	printf("\n\t\t\tTamano: %d Bytes.", p_size);
	printf("\n\t\t\tDimensiones: %d x %d.", portadora->width, portadora->height);

	printf("\n\n\tInformacion:\tNombre de Archivo: Informacion.png");
	printf("\n\t\t\tTamano: %d Bytes.", i_size);
	printf("\n\t\t\tDimensiones: %d x %d.", informacion->width, informacion->height);

	printf("\n\n\tSalida:\t\tNombre de Archivo: Salida.png");
	printf("\n\t\t\tTamano: %d Bytes.", s_size);
	printf("\n\t\t\tDimensiones: %d x %d.", salida->width, salida->height);

	printf("\n\n\tRecuperada:\tNombre de Archivo: Recuperada.png");
	printf("\n\t\t\tTamano: %d Bytes.", r_size);
	printf("\n\t\t\tDimensiones: %d x %d.", recuperada->width, recuperada->height);

	p_size = Portadora->imageSize;
	i_size = informacion->imageSize;

	aux = (float(i_size) * 100) / float(p_size);
	printf("\n\n\tPorcentaje de bytes modificados en la portadora: %f %%", aux);
	printf("\n\n\tCantidad de Least Significant Bits utilizados: %d", (unsigned int)cbits);

	printf("\n\n\tPresione una tecla para volver al menu.\n\n");

}

void Resultados(IplImage* portadora, FILE* informacion, IplImage* salida, FILE* recuperada)
{
	int p_size, i_size, s_size, r_size;
	float aux;

	p_size = GetFileSize("Portadora.png");
	i_size = GetFileSize("Informacion.txt");
	s_size = GetFileSize("Salida.png");
	r_size = GetFileSize("Recuperada.txt");

	system("cls");		// Windows
	//system("clear");	// Linux
	printf("\n\n\t\t\tResumen del Proceso\n");

	printf("\n\tPortadora:\tNombre de Archivo: Portadora.png");
	printf("\n\t\t\tTamano: %d Bytes.", p_size);
	printf("\n\t\t\tDimensiones: %d x %d.", portadora->width, portadora->height);

	printf("\n\n\tInformacion:\tNombre de Archivo: Informacion.txt");
	printf("\n\t\t\tTamano: %d Bytes.", i_size);
	//printf("\n\t\t\tDimensiones: %d x %d.", informacion->width, informacion->height);

	printf("\n\n\tSalida:\t\tNombre de Archivo: Salida.png");
	printf("\n\t\t\tTamano: %d Bytes.", s_size);
	printf("\n\t\t\tDimensiones: %d x %d.", salida->width, salida->height);

	printf("\n\n\tRecuperada:\tNombre de Archivo: Recuperada.txt");
	printf("\n\t\t\tTamano: %d Bytes.", r_size);
	//printf("\n\t\t\tDimensiones: %d x %d.", recuperada->width, recuperada->height);

	p_size = Portadora->imageSize;
	//i_size = informacion->imageSize;

	aux = (float(i_size) * 100) / float(p_size);
	printf("\n\n\tPorcentaje de bytes modificados en la portadora: %f %%", aux);
	printf("\n\n\tCantidad de Least Significant Bits utilizados: %d", (unsigned int)cbits);
	printf("\n\n\tPresione una tecla para volver al menu.\n\n");
}

/***************************************************************************
*							FIN INTERFAZ DE USUARIO
****************************************************************************/

/***************************************************************************
*						INICIO UTILIDADES
****************************************************************************/

/***************************************************************************
* Descripcion: abre una ventana para seleccionar un archivo
****************************************************************************/
int OpenFileDialog(int textFileAllowed)
{
	TCHAR FileName[MAX_FILE_NAME];
	Vaciar((char *)FileName, MAX_FILE_NAME, sizeof(TCHAR));
	OPENFILENAME  ofn;
	memset(&ofn, 0, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL;
	ofn.hInstance = NULL;
	if(textFileAllowed == 0)ofn.lpstrFilter = L"Imagenes\0*.jpg;*.png;*.tif\0All(*.*)\0*.*\0\0";
	else ofn.lpstrFilter = L"Imagenes\0*.jpg;*.png;*.tif\0Texto\0*.txt\0All(*.*)\0*.*\0\0";
	ofn.lpstrFile = (LPTSTR)FileName;
	ofn.nMaxFile = MAX_FILE_NAME;
	ofn.lpstrTitle = L"Por favor seleccione un archivo";
	ofn.Flags = OFN_NONETWORKBUTTON | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	if(!GetOpenFileName(&ofn)){return 0;}
	PathToWindows(FileName);
	return 1;
}

/***************************************************************************
* Descripcion: reordena el path para que funcione en Windows
****************************************************************************/
void PathToWindows(TCHAR* text)
{
	Vaciar(NombreArchivo,MAX_FILE_NAME,sizeof(char));
	//Reemplazo la barra por la doble barra invertida
	int i = 0;
	while (text[i] != '\0')
	{
		if(text[i] == '\\'){text[i] = '/';}
		i++;
	}
	//Copio el path a NombreArchivo
	i = 0;
	while (text[i] != '\0')
	{
		NombreArchivo[i] = (char)text[i];
		i++;
	}
}

/***************************************************************************
* Descripcion: Funci�n que realiza el Histograma
****************************************************************************/
int GetHistograma(IplImage *img, unsigned int *Histovector)
{
	int alto = img->height;
	int ancho = img->width;
	int Fila = 0, Columna = 0, max = 0;
	CvScalar imgscalar;
	for (Fila = 0; Fila < alto; Fila++)
	{
		for (Columna = 0; Columna < ancho; Columna++)
		{
			imgscalar = cvGet2D(img, Fila, Columna);
			Histovector[(unsigned char)imgscalar.val[0]]++;
		}
	}
	max = GetMaximo(Histovector, 256);
	if (max == 0)
		max = 1;
	return max;
}

/***************************************************************************
* Descripcion: Funcion que busca el m�ximo en un vector
****************************************************************************/
int GetMaximo(unsigned int a[], unsigned int cant)
{
	unsigned int i, max = 0;
	for (i = 0; i<cant; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
		}
	}
	return(max);
}

/***************************************************************************
* Descripcion: Funci�n que pone en cero un vector
****************************************************************************/
void Vaciar(unsigned int a[], unsigned int cant)
{
	unsigned int i;
	for (i = 0; i < cant; i++)
		a[i] = 0;
}

void Vaciar(char* a, unsigned int cant, unsigned int tamanio)
{
	unsigned int i;
	for (i = 0; i < cant*tamanio; i++)
		*(a + i) = 0;
}

/***************************************************************************
* Descripcion: Funci�n que calcula el tama�o en bytes de un archivo
****************************************************************************/
int GetFileSize(char * filename)
{
	FILE * fp;
	int size;
	int err;
	err  = fopen_s(&fp,filename,"rb");
	if (err != 0) { fputs("File error", stderr); return 0; }
	if (fp == NULL) { fputs("File error", stderr); return 0; }

	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	rewind(fp);

	fclose(fp);

	return size;
}

/***************************************************************************
*							FIN UTILIDADES
****************************************************************************/